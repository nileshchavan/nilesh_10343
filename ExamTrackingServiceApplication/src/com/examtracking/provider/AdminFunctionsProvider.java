package com.examtracking.provider;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.examtracking.bean.SetLoginDetails;
import com.examtracking.bean.SetMarkSheet;
import com.examtracking.bean.StudentDetails;
import com.examtracking.dao.LoginDetailsData;
import com.examtracking.dao.StudentData;
import com.examtracking.service.ExamDetailsImpl;
import com.examtracking.service.UserLoginImpl;
import com.examtracking.util.JwtImpl;
import com.examtracking.view.DisplayStudentInfo;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/AdminFunctionsProvider")
public class AdminFunctionsProvider {
	
	
	@GET
	@Path("/searchbyid")
	@Produces("application/json")
	@Consumes("application/json")
	public String getStudentById(@QueryParam("stud_id") long stud_id) {
		Gson gson = new Gson();

		StudentDetails result = new StudentData().getStudentByid(stud_id);

		String studentJsonString = gson.toJson(result);

		return studentJsonString;

	}
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/updateStudent")
	@Produces(MediaType.APPLICATION_JSON)
	public String updateStudent(StudentDetails student) {
	
		boolean result = ExamDetailsImpl.upDateStudent(student);
		
		if (result) {
		return "student_details updated";
		} 
		return "error";
	}
	/**
	 * This is resource method which performs register the student operation
	 * 
	 * @param student json object
	 * @return
	 */
	@POST
	@Path("/registerStudent")
	@Produces("application/json")
	@Consumes("text/plain")
	public String registerStudent(String student) {
		String str = null;
		Gson gson = new Gson();
		StudentDetails studentData = gson.fromJson(student, StudentDetails.class);		
		boolean result = new ExamDetailsImpl().registerNewStudent(studentData);
		if (result) {
			str = "student_record_inserted";
		}
		return str;
	}
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/setLoginDetails")
	@Produces("text/plain")
	public String setLoginDetails(String student) {

		String result = null;
		Gson gson = new Gson();

		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(student);
		JsonObject jsonObject = jsonTree.getAsJsonObject();

		JsonElement f1 = jsonObject.get("stud_id");
		long stud_id = f1.getAsLong();

		List<SetLoginDetails> list = new ArrayList<SetLoginDetails>();

		SetLoginDetails studentData = gson.fromJson(student, SetLoginDetails.class);
		studentData.setStud_id(stud_id);
		list.add(studentData);

		result = UserLoginImpl.SignUpStudent1(stud_id);

		if (result.equalsIgnoreCase("true")) {
			result = LoginDetailsData.saveStudentRecord(list);
		}

		if (result != null) {
			result = "student_login_details_Set";
		}
		return result;
	}

	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/displaystudentdetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<StudentDetails>  displayStudentDetails() {
	
		List<StudentDetails> studentDetails = DisplayStudentInfo.printStudentAllRecords();
		
		if (studentDetails!=null) {
		return studentDetails;
		} 
		return null;
	}
	/**
	 * This resource method gets all students from database
	 * @return list of students
	 */
	@GET
	@Path("/getAllStudents")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getAllStudents() {
		List<StudentDetails> list = new ExamDetailsImpl().getAllStudents();
		Gson gson = new Gson();
		String studentJsonString = gson.toJson(list);
		return studentJsonString;
	}
	
	/**
	 * This resource method gets all students from database
	 * @return list of students
	 */
	@GET
	@Path("/getMarksById")
	@Produces(MediaType.APPLICATION_JSON)
	public String getMarksById(String exam) {
		JsonObject jsonObject = new JwtImpl().getOriginalElement(exam);
		JsonElement f1 = jsonObject.get("exam_id");
		int ex_id = f1.getAsInt();
		
		Gson gson = new Gson();
		List<SetMarkSheet> list = new ExamDetailsImpl().getMarkSheetByExamId(ex_id);
		String studentJsonString = gson.toJson(list);
		return studentJsonString;
	}
	
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@GET
	@Path("/getRankOfStudents")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String getRankOfStudents (String exam) {
		JsonObject jsonObject = new JwtImpl().getOriginalElement(exam);
		JsonElement f1 = jsonObject.get("exam_id");
		int exam_id = f1.getAsInt();
		System.out.println(exam_id);
		List<String> list = new ExamDetailsImpl().displayAllStudentByRank(exam_id);
		
		Gson gson = new Gson();
		String studentJsonString = gson.toJson(list);
		return studentJsonString;
	}
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/displayLoginDetails")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<SetLoginDetails> printlogindetails () {
	
		List<SetLoginDetails> academic = DisplayStudentInfo.printLoginAllRecords();
		
		if (academic!=null) {
		return academic;
		} 
		return null;
	}
	
	
}
