package com.examtracking.provider;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.auth0.jwt.JWT;
import com.examtracking.bean.AdminLoginDetails;
import com.examtracking.bean.SetLoginDetails;
import com.examtracking.bean.StudentDetails;

import com.examtracking.dao.LoginDetailsData;
import com.examtracking.dao.StudentData;
import com.examtracking.service.ExamDetailsImpl;
import com.examtracking.service.UserLoginImpl;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Path("/adminfunctions")
public class LoginProvider {
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/adminlogin")
	@Produces("text/plain")
	public String login(@QueryParam("username") String username, @QueryParam("password") String password) {

		boolean result = false;
		UserLoginImpl login = new UserLoginImpl();
		result = login.loginUser(username, password);
		if (result) {
			return "login successful";
		} else {
			return "login unsuccessful";
		}
	}

	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/studentlogin")
	@Produces("text/plain")
	public String studentLogin(@QueryParam("username") String username, @QueryParam("password") String password) {

		boolean result = false;
		UserLoginImpl login = new UserLoginImpl();
		result = login.studentLoginDetails1(username, password);
		if (result) {
			return "login successful";
		} else {
			return "login unsuccessful";
		}
	}
	
	
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/studentcheckprevious")
	@Produces("text/plain")
	public String checkPreviousPassword(String resetLogin) {

		boolean result = false;
		
		Gson gson = new Gson();
		
		SetLoginDetails studentData = gson.fromJson(resetLogin, SetLoginDetails.class);
		String username=studentData.getUsername();
		String password=studentData.getPassword();
		result = UserLoginImpl.checkPrevious(username, password);
		
		if (result) {
		return "true";
		} 
		return "false";
	}

	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/studentresetpassword")
	@Produces("text/plain")
	public String resetPassword(String resetLogin) {
		boolean result = false;
		Gson gson = new Gson();

		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(resetLogin);
		JsonObject jsonObject = jsonTree.getAsJsonObject();

		JsonElement f1 = jsonObject.get("stud_id");
		long stud_id = f1.getAsLong();
		
		List<SetLoginDetails> list=new ArrayList<SetLoginDetails>();
		SetLoginDetails studentData = gson.fromJson(resetLogin, SetLoginDetails.class);
		studentData.setStud_id(stud_id);
		studentData.setUsername(studentData.getUsername());
		studentData.setPassword(studentData.getPassword());
		studentData.setSecurityQues(studentData.getSecurityQues());
		list.add(studentData);
		 result = UserLoginImpl.resetPassword(list,stud_id);
		
		if (result) {
		return "student_password_updated";
		} 
		return "error";
	}
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/studentForgotPassword")
	@Produces("text/plain")
	public String studentForgotPassword(@QueryParam("username") String username, @QueryParam("security_ques") String security_ques) {
	
		SetLoginDetails logindetails = UserLoginImpl.studentResetPassword(username,security_ques);
		
		if (logindetails!=null) {
		return "student_credentials_found";
		} 
		return "error";
	}
	/**
	 * This is resource method which performs login operation
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/adminForgotPassword")
	@Produces("text/plain")
	public String adminForgotPassword(@QueryParam("username") String username, @QueryParam("security_ques") String security_ques) {
	
		AdminLoginDetails logindetails = UserLoginImpl.resetPassword(username,security_ques);
		
		if (logindetails!=null) {
		return "admin_credentials_found";
		} 
		return "error";
	}
}
