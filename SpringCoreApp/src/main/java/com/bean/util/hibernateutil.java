package com.bean.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class hibernateutil {
	private static SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {
		try {
			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build(); // configures
																										// hibernate.cfg.xml
			// Create MetadataSources
			MetadataSources metadataSources = new MetadataSources(serviceRegistry);
			// Create Metadata
			Metadata metadata = metadataSources.getMetadataBuilder().build();
			// Create SessionFactory
			sessionFactory = metadata.getSessionFactoryBuilder().build();
			return sessionFactory;
			
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
