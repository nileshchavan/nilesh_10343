package com.abc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.abc.bean.Student;
import com.bean.util.hibernateutil;
@Repository
public class StudentData {
	
	/**
	 * This Method Insert new student in db
	 * @param student
	 * @return
	 */
	public boolean saveStudent(Student student) {
		SessionFactory sessionFactory = hibernateutil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(student);
		tx.commit();
		session.close();
		return true;
	}
	
	/**
	 * This Method Insert new student in db
	 * @param student
	 * @return
	 */
	public Student getStudentById(int id) {
		SessionFactory sessionFactory = hibernateutil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Student std = session.get(Student.class, id);
		tx.commit();
		session.close();
		return std;
	}
	/**
	 * Return all student present in database
	 * @return list of student
	 */
	public List<Student> getAllStudent(){
		SessionFactory sessionFactory = hibernateutil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		List<Student> list  = session.createCriteria(Student.class).list();
		tx.commit();
		session.close();
		return list;
	}
}
