package com.abc.mainMethod;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Student;
import com.abc.controller.StudentController;

public class MainMethod {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("Select Operation !!!");
		System.out.println("1) Insert new Record");
		System.out.println("2) Search by Id");
		System.out.println("3) Get All Student");
		int choice = sc.nextInt();
		switch (choice) {
		case 1: {
			insert();
			break;
		}
		case 2: {
			search();
			break;
		}
		case 3: {
			getAll();
			break;
		}
		}
	}

	public static void insert() {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/resources/context.xml");
		
		Student std = (Student) context.getBean("std");
		StudentController studController = (StudentController) context.getBean(StudentController.class);
		
		studController.insertStudent(std);
		System.out.println("record saved");
	}

	public static boolean search() {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/resources/context.xml");
		StudentController studController = context.getBean(StudentController.class); 
		System.out.println("Enter Id");
		int id = sc.nextInt();
		Student std = studController.getStudentbyId(id);
		System.out.println("----------------------------------------");
		System.out.println(std.getStudentId()+" "+std.getName()+" "+std.getAge()+" "+std.getCity()+" "+std.getState());
		return true;
	}

	public static boolean getAll() {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/abc/resources/context.xml");
		StudentController studController = context.getBean(StudentController.class); 
		List<Student> list = studController.getAllStudent();
		for(Student temp : list) {
			System.out.println("----------------------------------------");
			System.out.println(temp.getStudentId()+" "+temp.getAge()+" "+temp.getName()+" "+temp.getCity()+" "+temp.getState());
		}
		
		return false;
	}
}