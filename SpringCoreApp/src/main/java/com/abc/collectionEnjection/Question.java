package com.abc.collectionEnjection;

import java.util.Iterator;
import java.util.List;


public class Question {
	private String name;
	private List<String> answer;
	
	public Question(String name, List<String> answer) {
		super();
		this.name = name;
		this.answer = answer;
	}
	public void display() {
		System.out.println(name);
		Iterator<String> itr = answer.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		System.out.println();
	}
}
