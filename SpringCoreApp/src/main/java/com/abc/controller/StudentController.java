package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.bean.Student;
import com.abc.service.StudentService;
@Controller
public class StudentController {
	@Autowired
	private StudentService studentService;

	public StudentService getStudentService() {
		return studentService;
	}

	public void setStudentService(StudentService studentService) {
		this.studentService = studentService;
	}
	/**
	 * This Method Insert new student in db
	 * @param student
	 * @return
	 */
	public boolean insertStudent(Student student) {
		boolean result = studentService.insertStudent(student);
		return result;	
	}
	
	/**
	 * get student by ID
	 * @param id
	 * @return Student Object
	 */
	public Student getStudentbyId(int id) {
		Student std = studentService.fetchStudentById(id);
		return std;
	}
	
	/**
	 * Return all student present in database
	 * @return list of student
	 */
	public List<Student> getAllStudent(){
		List<Student> list = studentService.getAllStudent();
		return list;
	}
}
