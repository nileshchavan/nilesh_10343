package com.abc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.entities.Account;

@Repository
public class AccountDaoImpl implements AccountDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override

	public Account getByAccNo(int accNo) {

		Session session = sessionFactory.getCurrentSession();

		Account account = session.get(Account.class, accNo);

		return account;
	}

	public boolean insertAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();

		int i = (int) session.save(account);

		boolean result = false;
		if (i > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public boolean deleteAccount(int accNo) {
		
		Session session = sessionFactory.getCurrentSession();	
		Account account = session.get(Account.class, accNo);
		session.delete(account);
		return true;
	}
	@Override
	public List<Account> getAllAccounts() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Account");
        List<Account> list = query.getResultList();
		return list;
	}

	@Override
	public String withdraw(int accNo, double amount) {
		Session session = sessionFactory.getCurrentSession();	
		Account account = session.get(Account.class, accNo);
		if(account.getBalance() <= amount) {
			return "insufficient account balance";
		}else {
			account.setBalance(account.getBalance() - amount);
			return "updated balance is "+account.getBalance();
		}
	}

	@Override
	public String deposite(int accNo, double amount) {
		Session session = sessionFactory.getCurrentSession();	
		Account account = session.get(Account.class, accNo);
		account.setBalance(account.getBalance() + amount);
		return "updated balance is "+account.getBalance();
	}

	@Override
	public void updateAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();	
		Account account1 = session.get(Account.class, account.getAccno());
		if(account.getBalance() != 0.0) {
			account1.setBalance(account.getBalance());
		}
		if(account.getName() != null) {
			account1.setName(account.getName());
		}
		session.update(account1);
	}

}
