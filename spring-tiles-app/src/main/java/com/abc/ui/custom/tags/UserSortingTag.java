package com.abc.ui.custom.tags;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.abc.bean.UserDetails;

public class UserSortingTag extends SimpleTagSupport {
	
	private List<UserDetails> userDetailsList;

	public List<UserDetails> getUserDetailsList() {
		return userDetailsList;
	}

	public void setUserDetailsList(List<UserDetails> userDetailsList) {
		this.userDetailsList = userDetailsList;
	}
	
	 @Override
	    public void doTag() throws JspException, IOException {
		 
		 	Collections.sort(userDetailsList, new FirstNameSorter());
		 
	        JspWriter out =  getJspContext().getOut();
	        out.println("<html><body>");
	        out.println("<table><tr><th>First Name</th><th>Last Name</th><th>Email</th></tr>");
	        for(UserDetails user: userDetailsList) {
	        out.println("<tr>");
	        out.println("<td>"+user.getFirstName()+"</td>");
	        out.println("<td>"+user.getLastName()+"</td>");
	        out.println("<td>"+user.getEmail()+"</td>");
	        out.println("</tr>");
	        }
	        out.println("</table>");
	        out.println("</body></html>");
	      
	    }


}

class FirstNameSorter implements Comparator<UserDetails>{

@Override
public int compare(UserDetails obj1, UserDetails obj2) {
	// TODO Auto-generated method stub
	return obj1.getFirstName().compareTo(obj2.getFirstName());
}
}
