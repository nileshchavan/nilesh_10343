package com.abc.service;

import java.util.List;

import com.abc.bean.UserDetails;

public interface ProfileService {

	int saveProfile(UserDetails userDetails);
	UserDetails searchByUsername(String username);
	List<UserDetails> fetchAllUsers();
}
