package com.abc.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.abc.bean.UserDetails;
import com.abc.exceptions.MyDaoException;
import com.ibatis.sqlmap.client.SqlMapClient;

@Repository
public class ProfileDAOImpl implements ProfileDAO {

	@Autowired
	private SqlMapClient sqlMapClient;

	public void setSqlMapClient(SqlMapClient sqlMapClient) {
		this.sqlMapClient = sqlMapClient;
	}

	@Override
	public int createProfile(UserDetails userDetails) throws DataAccessException {
		
		  try {
	            this.sqlMapClient.update("addUser", userDetails);
	        }
	        catch (SQLException ex) {
	            throw new MyDaoException(ex);
	        }
		return userDetails.getId();
	}

	@Override
	public UserDetails getUserByUserName(String username) {
		
		UserDetails userDetails =null;
		
		  try {
	            userDetails = (UserDetails) this.sqlMapClient.queryForObject("getUserByUserName", username);
	        }
	        catch (SQLException ex) {
	            throw new MyDaoException(ex);
	        }	
		  return userDetails;
	}

	@Override
	public List<UserDetails> getAllUsers() {
		List<UserDetails> usersList = new ArrayList<UserDetails>();
		 try {
	            usersList = (List<UserDetails>) this.sqlMapClient.queryForList("getAllUsers");
	        }
	        catch (SQLException ex) {
	            throw new MyDaoException(ex);
	        }	
		return usersList;
	}
	
	

}
