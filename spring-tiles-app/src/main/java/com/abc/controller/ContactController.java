package com.abc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.abc.bean.ContactDetails;

@SuppressWarnings("deprecation")
public class ContactController extends SimpleFormController {
	
	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		System.out.println("inside onsubmit");
		ContactDetails contactDetails = (ContactDetails) command;
		ModelAndView mav = new ModelAndView(getSuccessView());
		//ModelAndView mav = new ModelAndView(new RedirectView(getSuccessView()));
		//ModelAndView mav = new ModelAndView("redirect:redirect.html");
		mav.addObject("contact", contactDetails);
		return mav;
	}

	@Override
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		System.out.println("inside refdata");
		ContactDetails contactDetails = (ContactDetails) command;
		Map refData = new HashMap(); // refdata holds information that are not specific to the bean, like user logged in
		System.out.println("Errors:"+errors.getErrorCount());
		return refData;
	}

}
