<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="ui" uri="customeTagLib" %>
 <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<p>Date: <ui:newDate/></p>
 <form:form commandName="contactUser" method="post" name="contactUser"  >
   <table align="center" >
		<tr>
			<td>First Name :</td>
			<td><form:input path="firstName" /></td>
		</tr>
		<tr>
			<td>Last Name :</td>
			<td><form:input path="lastName" /></td>
		</tr>
		<tr>
			<td>DOB :</td>
			<td><form:input path="dateOfBirth" />(dd-MMM-yyyy)</td>
		</tr>		
		<tr>
			<td></td>
			<td><input type="submit" value="Submit" /></td>
		</tr>
	</table>
  </form:form> 

<!--  
<form action="contact.htm" method="post">
First Name :<input type="text" name="firstName"/><br>
Last Name :<input type="text" name="lastName"/><br>
DOB :<input type="text" name="dateOfBirth"/><br>
<input type="submit" value="submit"/><br>
</form>
-->
</body>
</html>