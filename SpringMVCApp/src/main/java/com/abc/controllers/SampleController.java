package com.abc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SampleController {

	@GetMapping("/sayHello")
	public String sayHello(ModelMap map) {
		String data =  "Welcome to Spring MVC using java configuration";
		map.addAttribute("modelData",data);
		return "sample";
		
	}
	
}
