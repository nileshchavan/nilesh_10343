<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Enter Details To Update</h2>
<form action="updateDetails">
<table>
<tr>
				<td>User Id</td>
				<td><input type="number" name="id" value="${userDetails.id }" readonly="readonly"/></td>
			</tr>
			<tr>
				<td>First Name</td>
				<td><input type="text" name="firstName" value="${userDetails.firstName }"/></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="lastName" value="${userDetails.lastName }" /></td>
			</tr>
			<tr>
				<td>User Name</td>
				<td><input type="text" name="username" value="${userDetails.username }"/></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="password" value="${userDetails.password }"/></td>
			</tr>
			<tr>
				<td>Organization</td>
				<td><input type="text" name="organization" value="${userDetails.organization }" /></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="text" name="email" value="${userDetails.email }"/></td>
			</tr>
			<tr>
				<td>Phone</td>
				<td><input type="text" name="phone" value="${userDetails.phone }"/></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Update" /></td>
			</tr>
		</table>
</form>
<c:if test="${result eq true }">
<h4>Updated Successfully</h4>
</c:if>
<c:if test="${result eq false && attempt>0 }">
<h4>Updation Failed</h4>
</c:if>
</body>
</html>