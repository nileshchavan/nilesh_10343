package com.abc.service;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;

public interface ProfileService {

	void saveProfile(UserDetails userDetails);
	
	Profile searchProfile(String username);
	
	boolean updateDetails(UserDetails userDetails);

	int deleteProfile(String username);
}
