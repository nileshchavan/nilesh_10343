package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;
import com.abc.mappers.UserMapper;

@Service
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private UserMapper userMapper;
	@Override
	@Transactional
	public void saveProfile(UserDetails userDetails) {
		
		 userMapper.insertUser(userDetails);
	}
	
	@Override
	@Transactional
	public Profile searchProfile(String username) {
		// TODO Auto-generated method stub
		return userMapper.getUserUsingUsername(username);
	}

	@Override
	@Transactional
	public boolean updateDetails(UserDetails userDetails) {
		Profile profile = userMapper.getUserUsingUid(userDetails.getId());
		boolean status = false;
		if(userDetails.getEmail() != profile.getEmail())
		{
			status = true;
			profile.setEmail(userDetails.getEmail());
		}
		if(userDetails.getFirstName() != profile.getFirstName())
		{
			status = true;
			profile.setFirstName(userDetails.getFirstName());
		}
		if(userDetails.getLastName() != profile.getLastName())
		{
			status = true;
			profile.setLastName(userDetails.getLastName());
		}
		if(userDetails.getOrganization() != profile.getOrganization())
		{
			status = true;
			profile.setOrganization(userDetails.getOrganization());
		}
		if(userDetails.getPhone() != profile.getPhone())
		{
			status = true;
			profile.setPhone(userDetails.getPhone());
		}
		if(userDetails.getUsername() != profile.getUsername())
		{
			status = true;
			profile.setUsername(userDetails.getUsername());
		}
		userMapper.updateUser(profile);
		return status;
	}

	@Override
	@Transactional
	public int deleteProfile(String name) {
		return userMapper.deleteUser(name);
		
	}

}
