package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;
import com.abc.service.ProfileService;

@Controller
public class UserDetailsController {
	
	@Autowired
	private ProfileService profileService;
	
	@RequestMapping("/regform")
	public String getForm() {
		return "regform";		
	}
	

	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String saveUser(@ModelAttribute UserDetails userDetails, ModelMap map) {
		profileService.saveProfile(userDetails);
		map.addAttribute("userDetails", userDetails.getFirstName());
		return "success";		
	}
	
	@RequestMapping("/edit")
	public String getFormedit() {
		return "edit";		
	}
	
	

	@RequestMapping(value="/editsearch")
	public String searchForEdit(@ModelAttribute UserDetails userDetails, ModelMap map) {
		Profile result = profileService.searchProfile(userDetails.getUsername());
		map.addAttribute("userDetails", result);
		map.addAttribute("attempt", 1);
		return "edit";		
	}
	
	@RequestMapping(value="/updateDetails")
	public String updateDetails(@ModelAttribute UserDetails userDetails, ModelMap map)
	{
		boolean result = profileService.updateDetails(userDetails);
		map.addAttribute("result", result);
		map.addAttribute("attempt", 1);
		return "updatedetails";
	}

	@RequestMapping("/deleteform")
	public String getDeleteForm() {
		return "deleteform";
	}

	@RequestMapping(value = "/delete")
	public String getDeleteForm(@RequestParam String username,ModelMap map) {
		int result = profileService.deleteProfile(username);
        String showMsg = "";
		
		if (result != 0)
			showMsg = "deleted  successefully";
		else
			showMsg= "No user found with name "+username;
		
		map.put("deletestatus", showMsg);	
		
		return "deleteform";
	}

}
