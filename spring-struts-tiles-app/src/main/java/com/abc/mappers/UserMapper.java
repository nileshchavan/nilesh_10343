package com.abc.mappers;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import com.abc.bean.UserDetails;
import com.abc.hibernate.entities.Profile;

@Component
public interface UserMapper {
	
	@Results({
        @Result(property = "id", column = "id")
	})
	
	@Select("SELECT * FROM user_details_tbl WHERE id = #{id}")
    public Profile getUserUsingUid(@Param("id") long id);
     
    @Select("SELECT * FROM user_details_tbl WHERE username = #{username}")
    public Profile getUserUsingUsername(@Param("username") String username);
    
    @Insert("INSERT into user_details_tbl(first_name,last_name,username,password,organization,email,phone) VALUES(#{firstName}, #{lastName},#{username},#{password},#{organization},#{email},#{phone})")
	void insertUser(UserDetails user);
    
    @Update("UPDATE user_details_tbl SET first_name=#{firstName}, phone =#{phone},email=#{email},password=#{password},last_name=#{lastName},organization=#{organization} WHERE id =#{id}")
	void updateUser(Profile user);
	
	@Delete("DELETE FROM user_details_tbl WHERE username =#{username}")
	int deleteUser(String  username);
}
