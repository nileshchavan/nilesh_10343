package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;


public class StudentServiceImpl implements StudentService{

	/**
	 * This method get student object as parameter and call insert method from DAO
	 * @param student object
	 */
	public boolean insertStudent(Student student) {
		StudentDAO studentDAO = new StudentDAOImpl();
		
		//calling createStudent methid which insert new student into database
		boolean result = studentDAO.createStudent(student);
		
		//if inserted successfully return true
		return result;
	}

	/**
	 * This method search student based on id 
	 * @param student id
	 */
	public Student findById(int id) {
		StudentDAO studentDAO = new StudentDAOImpl();
		//calling DAO searchById method which search student in databse
		Student student = studentDAO.searchById(id);
		
		//return student object
		return student;
	}
	/**
	 * This Method gets all student from database and shows to user
	 * @return student list
	 */
	@Override
	public List<Student> fetchAllStudents() {
		
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}
	/**
	 * This method update student based on given id
	 * @param id
	 * @param age
	 * @param name
	 * @return true/false
	 */
	public boolean UpdateById(int id , int age, String name) {
		
		StudentDAOImpl studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.UpdateById(id,age,name);
		return result;
	}
	/**
	 * This Method delete student from database based on given id
	 * @param id
	 * @return true / false
	 */
	public boolean DeleteById(int id) {
		
		StudentDAOImpl studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.DeleteById(id);
		return result;
	}

}














