package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		PrintWriter out = response.getWriter();
		
		//setting json content type
		response.setContentType("application/json");
		
		response.setCharacterEncoding("UTF-8");
		
		StudentService service = new StudentServiceImpl();

		// calling findbyId method of service classs
		Student student = service.findById(1);
		
		String studentJsonString = this.gson.toJson(student);

		// writting object into stream
		out.print(studentJsonString);
		
		out.flush();
	}

}
