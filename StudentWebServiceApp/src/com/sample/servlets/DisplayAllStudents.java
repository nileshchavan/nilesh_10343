package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
	/**
	 * This method get student list and display all student records
	 */
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   			
   			response.setContentType("application/json");
   			response.setCharacterEncoding("UTF-8");
   			//calling student service
   	   		StudentService service = new StudentServiceImpl();

   			// calling method which fetch all student present in database
   			List<Student> studentList = service.fetchAllStudents();
   			
   			PrintWriter out = response.getWriter();
   			
   			//converting student list object into json object 
   			String employeeJsonString = this.gson.toJson(studentList);
   			
   			//writting object into stream
   			out.print(employeeJsonString);
   	        out.flush();
   			

   		
   		
   	}
}
