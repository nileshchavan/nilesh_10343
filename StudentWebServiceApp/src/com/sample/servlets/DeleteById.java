package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteById
 */
@WebServlet("/DeleteById")
public class DeleteById extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * This method delete student by using id
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(isLoginUser(request,response)) {
			PrintWriter out = response.getWriter();
			int id = 0;
			try {
				id = Integer.parseInt(request.getParameter("id"));
			} catch(Exception e) {
				out.print("<html><body>");
				out.print("<label style='color: red;'>Please Enter Student Id</label>");
				out.print("</body></html>");
				//if user did not entered id then asking again
				RequestDispatcher rd = request.getRequestDispatcher("update.html");
				rd.include(request, response);
			}
			
			//calling delete student and parameter is student Id
			boolean result = new StudentServiceImpl().DeleteById(id);
			
			
			//if student record deleted successfully 
			if(result) {
				out.print("<html><body>");
				out.println("<h4>Student Deleted.</h4>");
				out.print("<hr><a href='index.html'>Back to Home</a>");
				out.print("</body></html>");
				
				//if student record  not found 
			}else {
				out.print("<html><body>");
				out.println("<h4>Student not found!!. Enter correct student id</h4>");
				out.print("<hr><a href='index.html'>Back to Home</a>");
				out.print("</body></html>");
			}
		} else {
			response.sendRedirect("error_login_user.html");
		}
		
	}
	public boolean isLoginUser(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession(false);
   		String name = null;
   		boolean result = false;
   		try {
   			name = (String) session.getAttribute("user");
   		}catch(NullPointerException e) {
   			result = false;
   		}
   		if(name != null) {
   			result = true;
   		}
		return result;
	}

}
