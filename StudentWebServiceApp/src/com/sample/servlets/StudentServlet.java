package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String objarray = request.getParameter("student").toString();
		GsonBuilder builder = new GsonBuilder();
	    builder.setPrettyPrinting();
	    Gson gson = builder.create();
	    
		Student student = gson.fromJson(objarray, Student.class);
		
		Address address = gson.fromJson(objarray, Address.class);
		
		int id = student.getId();
		System.out.println(address.getState());
		//creating student object
		student.setId(student.getId());
		student.setName(student.getName());
        student.setAge(student.getAge());
        student.setAddress(address);
		
        //passing student object to insertStudent method
        
        StudentService service = new StudentServiceImpl();
        boolean result = service.insertStudent(student);
		System.out.println(result);	
	}

}
