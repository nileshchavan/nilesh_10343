package com.bank.Aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LoggingAspect {

//	 private Logger logger = LoggerFactory.getLogger(this.getClass());
	 
	 @Before("execution(* com.bank.service.AccountServiceImpl.searchByAccNo(..))")
	    public void before(JoinPoint joinPoint) {
		 System.out.println("Before Call "+ joinPoint.getSignature().getName()+" With Parameter "+ Arrays.toString(joinPoint.getArgs()));
	 }
	 
	 @After("execution(* com.bank.service.AccountServiceImpl.*(..))")
	    public void after(JoinPoint joinPoint) {
		 System.out.println("After Call "+ joinPoint.getSignature().getName());
	 }
	 
	 @AfterReturning("execution(* com.bank.service.AccountServiceImpl.deleteAccount*(..))")
	    public void afterReturning(JoinPoint joinPoint) {
		 System.out.println("After Returning method "+ joinPoint.getSignature().getName());
	 }
	 
	 @AfterThrowing("execution(* com.bank.service.AccountServiceImpl.deleteAccount*(..))")
	    public void afterException(JoinPoint joinPoint) {
		 System.out.println("After Exception Occur method "+ joinPoint.getSignature().getName());
	 }
}
