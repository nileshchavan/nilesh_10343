package com.bank.service;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bank.dao.AccountDaoImpl;
import com.bank.entities.Account;


@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountDaoImpl accountDao;
	
	
	@Override
	public Account searchByAccNo(String accNo) {
		Account account = accountDao.findByAccno(accNo);
		
		Account acc = new Account();
		acc.setAccno("1001");
		acc.setName("Nilesh");
		acc.setBalance(25000);
		return acc;
	}


	@Override
	public Account insertAccount(Account account) {
		Account newAcc = accountDao.save(account);
		return newAcc;
	}


	@Override
	public boolean deleteAccount(int accNo) {
		
			int i  = 100 / 0;
			return true;
	}


	@Override
	public List<Account> getAllAccounts() {
		Account acc1 = new Account();
		acc1.setAccno("1001");
		acc1.setName("Nilesh");
		acc1.setBalance(25000);
		
		Account acc = new Account();
		acc.setAccno("1002");
		acc.setName("Harish");
		acc.setBalance(35000);
		
		List<Account> list = new ArrayList<Account>();
		list.add(acc1);
		list.add(acc);
		return list;
	}

//	@Transactional
//	@Override
//	public boolean deleteAccount(int accNo) {
//		// TODO Auto-generated method stub
//		
//		return accountDao.deleteAccount(accNo);
//	}
//
//	@Transactional
//	@Override
//	public List<Account> getAllAccounts() {
//		List<Account> list = accountDao.getAllAccounts();
//		return list;
//	}
//
//	@Transactional
//	@Override
//	public String withdraw(int accNo, double amount) {
//		String message = accountDao.withdraw(accNo, amount);
//		return message;
//	}
//
//	@Transactional
//	@Override
//	public String deposite(int accNo, double amount) {
//		String message = accountDao.deposite(accNo, amount);
//		return message;
//	}
//	@Transactional
//	@Override
//	public void updateAccount(Account account) {
//		accountDao.updateAccount(account);
//	}

}
