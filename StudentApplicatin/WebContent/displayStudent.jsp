<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.util.*" %>
    <%@ page import="com.sample.bean.Student" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		//getting previously created session object
		HttpSession ses = request.getSession(false);
		
		//getting student list object presents in stdList from session object
		List<Student> list = (List<Student>) ses.getAttribute("stdList");
		
	%>
	<table border='1' width='30%' style='border-style: inset;border-color: coral;border-width: 3px'>
		<caption>All Students</caption>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Age</th>
		</tr>
		<%
			//printing all student using HTML tables
			Iterator<Student> itr = list.iterator();
			while(itr.hasNext()) {
				Student std = itr.next();
				%>
				<tr>
					<td><%= std.getId() %></td>
					<td><%= std.getName() %></td>
					<td><%= std.getAge() %></td>
				</tr>
			<% }%>
			
	</table>
	<hr>
	
	<a href='index.html'>Back to Home</a>
</body>
</html>