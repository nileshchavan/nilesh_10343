<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	
	<table border='1' width='30%' style='border-style: inset;border-color: coral;border-width: 3px'>
	 <caption>Student Table</caption>
	 <tr>
	 	<th>Id</th>
	 	<th>Name</th>
	 	<th>Age</th>
	 	<th>City</th>
	 	<th>State</th>
	 </tr>
	 <tbody>
	 	<tr>
	 		<th>${student.id}</th>
	 		<th>${student.name}</th>
	 		<th>${student.age}</th>
	 		<th>${student.address.city}</th>
	 		<th>${student.address.state}</th>
	 	</tr>
	 </tbody>
	</table><hr>
		<% session.setMaxInactiveInterval(30); %>
	<a href='index.html'>Back to Home</a>
</body>
</html>