package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * This method get student list and display all student records
	 */
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   
   		HttpSession session = request.getSession(false);
   		//Students(request,response,session);
   		if(session != null) {
   	   		StudentService service = new StudentServiceImpl();

   			// calling method which fetch all student present in database
   			List<Student> studentList = service.fetchAllStudents();
   			

   			// Getting Request Dispatcher to dispatch request to displayStudent.jsp
   			RequestDispatcher rd = request.getRequestDispatcher("displayStudent.jsp");

   			// setting attribute to session scope
   			session.setAttribute("stdList", studentList);

   			// Forwarding request
   			rd.forward(request, response);
   		}else {
   			response.sendRedirect("error_login_user.html");
   		}
   		
   	}
}
