package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = 0;
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(false);
   		//Students(request,response,session);
   		
   		if(session != null) {
   			try {
   				
   				//getting student id from html page
   				id = Integer.parseInt(request.getParameter("searchid"));
   			}catch(Exception e) {
   				out.print("<html><body>");
   				out.print("<label style='color: red;'>Please Enter Student Id</label>");
   				out.print("</body></html>");
   				
   				//if user did not entered id then asking again
   				RequestDispatcher rd = request.getRequestDispatcher("searchbyid.html");
   				rd.include(request, response);
   			}
   			
   			StudentService service = new StudentServiceImpl();
   			
   			//calling findbyId method of service classs
   			Student student = service.findById(id);
   			
   			RequestDispatcher rd = request.getRequestDispatcher("search_by_id.jsp");
   			request.setAttribute("student", student);
   			rd.forward(request, response);
   		}else {
   			response.sendRedirect("error_login_user.html");
   		}
		
	}

}
