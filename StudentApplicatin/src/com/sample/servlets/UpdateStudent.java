package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		int id = 0;
		try {
			id = Integer.parseInt(request.getParameter("id"));
			//maintaining id , to used in next servlet
			HttpSession ses = request.getSession(true);
			ses.setAttribute("id", id);
		} catch(Exception e) {
			out.print("<html><body>");
			out.print("<label style='color: red;'>Please Enter Student Id</label>");
			out.print("</body></html>");
			//if user did not entered id then asking again
			RequestDispatcher rd = request.getRequestDispatcher("update.html");
			rd.include(request, response);
		}
		
		
	
		//checking whether student present or not
		if(ckeckStudent(id)) {
			
			//if student present sending request to update student html page
			RequestDispatcher rd = request.getRequestDispatcher("updateStudent.html");
			rd.forward(request,response);
		} else {
			
			// if student not found	
			out.print("<html><body>");
			out.println("<h4>Student not found!!. Enter correct student id</h4>");
			out.print("<hr><a href='index.html'>Back to Home</a>");
			out.print("</body></html>");
		}
	}
	
	/**
	 * This method return true if student found in database
	 * @param id
	 * @return
	 */
	public boolean ckeckStudent(int id) {
		boolean result = false;
		StudentService std = new StudentServiceImpl();
		//this method return student if present in database
		Student student = std.findById(id);
		if(student != null) {
			result = true;
		}else {
			result = false;
		}
		return result;
	}

}
